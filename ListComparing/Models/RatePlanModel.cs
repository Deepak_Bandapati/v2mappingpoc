﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ListComparing.Models
{
    public class Rate
    {
        public decimal rateVal { get; set; }
        public decimal? extraAdult { get; set; }
        public decimal? extraChild { get; set; }
    }

    public class SeasonRates
    {
        public int roomClassId { get; set; }
        public int maxAdults { get; set; }
        public int maxPersons { get; set; }
        public string name { get; set; }
        public decimal rate { get; set; }
    }

    public class Seasons
    {
        public string name { get; set; }
        public List<SeasonRates> seasonRates { get; set; }
        public int clientID { get; set; }
        public int propertyId { get; set; }
        public List<DateTime> seasonDates { get; set; }
    }
    public class RatePlan
    {
        public int ratePlanId { get; set; }
        public string name { get; set; }
        public int clientId { get; set; }
        public int propertyId { get; set; }
        public int ratePlanTypeId { get; set; }
        public List<ratePlanChannels> ratePlanChannels { get; set; }
        public List<Seasons> seasons { get; set; }
    }

    public class ratePlanChannels {
        public int ratePlanChannelId { get; set; }
        public int ratePlanId { get; set; }
        public int channelId { get; set; }
        public int mealPlanId { get; set; }
    }
}