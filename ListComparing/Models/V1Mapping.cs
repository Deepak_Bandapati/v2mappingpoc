﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ListComparing.Models
{
    public class TestV2RatePlanMapping
    {
        public int V1RatePlanId { get; set; }
        [Key]
        public int V2RatePlanId { get; set; }
    }
    public class Client_list_lov
    {
        [Key]
        public int ListId { get; set; }
        public int ClientId { get; set; }
        public string ListName { get; set; }
        public string ListDesc { get; set; }
        public int ListValue { get; set; }
        public int ListType { get; set; }
        public Boolean isSystemList { get; set; }
        public int Active { get; set; }
        public int UserUpdate { get; set; }
        public DateTime dateUpdate { get; set; }
    }
    public class Property_list_lov
    {
        public int ListId { get; set; }
        public int ClientId { get; set; }
        public int PropertyId { get; set; }
        public bool Active { get; set; }
        public int UserUpdate { get; set; }
        public DateTime dateUpdate { get; set; }
    }
    public class Rate_ref
    {
        [Key]
        public int rateId { get; set; }

        public int? propertyid { get; set; }

        public string rateName { get; set; }

        public string rateDisplayName { get; set; }

        public decimal? amount { get; set; }

        public bool? isProrated { get; set; }

        public decimal? proRateAmount { get; set; }

        public decimal? perAddAdult { get; set; }

        public decimal? perAddChild { get; set; }

        public decimal? proRatePerAddAdult { get; set; }

        public decimal? proRatePerAddChild { get; set; }

        public bool? allowStayThrough { get; set; }

        public int? minStay { get; set; }

        public int? rateInterval { get; set; }

        public bool? checkInMonday { get; set; }

        public bool? checkInTuesday { get; set; }

        public bool? checkInWednesday { get; set; }

        public bool? checkInThursday { get; set; }

        public bool? checkInFriday { get; set; }

        public bool? checkInSaturday { get; set; }

        public bool? checkInSunday { get; set; }

        public int? maxAdults { get; set; }

        public int? maxPersons { get; set; }

        public bool? lowestRate { get; set; }

        public bool? rackRate { get; set; }

        public string ratePromoCode { get; set; }

        public int? Active { get; set; }

        public int? ratePlanID { get; set; }

        public string rateDescription { get; set; }

        public string ratePolicy { get; set; }

        public int? userUpdate { get; set; }

        public DateTime? dateUpdate { get; set; }

        public int? rateTypeID { get; set; }

        public int? basedOnRateID { get; set; }

        public bool? baseAmount_offset_percent { get; set; }

        public decimal? baseAmount_offset_absolute { get; set; }

        public bool? proRateAmount_offset_percent { get; set; }

        public decimal? proRateAmount_offset_absolute { get; set; }

        public bool? perAddAdult_offset_percent { get; set; }

        public decimal? perAddAdult_offset_absolute { get; set; }

        public bool? proRatePerAddAdult_offset_percent { get; set; }

        public decimal? proRatePerAddAdult_offset_absolute { get; set; }

        public bool? perAddChild_offset_percent { get; set; }

        public decimal? perAddChild_offset_absolute { get; set; }

        public bool? proRatePerAddChild_offset_percent { get; set; }

        public decimal? proRatePerAddChild_offset_absolute { get; set; }

        public int? maxStay { get; set; }

        public int? minAdvanceBooking { get; set; }

        public int? maxAdvanceBooking { get; set; }

        public int? packageID { get; set; }

        public int? packageAvailabilityTypeID { get; set; }

        public bool? alwaysAvailable { get; set; }

        public bool? MonthlyRate { get; set; }

        public decimal? derivedOffsetAmount { get; set; }

        public int? derivedFromRatePlanID { get; set; }

        public bool? derivedIsPercent { get; set; }

        public bool? isSeasonsAlwaysAvailable { get; set; }

        public bool? isRoomclassAlwaysAvailable { get; set; }

        public bool? isSourceAlwaysAvailable { get; set; }

    }

    public class RatePlan_ref
    {
        [Key]
        public int ratePlanID { get; set; }
        public int mealPlanID { get; set; }
        public int? clientID { get; set; }
    }
    public class Rate_roomClass_xref
    {
        public int rateId { get; set; }
        public int roomClassId { get; set; }

        public int? Active { get; set; }

        public int? propertyID { get; set; }
    }
    public class Rate_season_xref
    {
        public int rateId { get; set; }
        public int seasonId { get; set; }

        public int? Active { get; set; }

        public int? propertyID { get; set; }
    }
    public class Rate_source_xref
    {
        public int rateId { get; set; }

        public int sourceID { get; set; }

        public int? Active { get; set; }

        public int? propertyID { get; set; }
    }
    public class Season_ref
    {
        [Key]
        public int seasonId { get; set; }

        public int? propertyid { get; set; }

        public string seasonName { get; set; }

        public DateTime dateStart { get; set; }

        public DateTime dateEnd { get; set; }

        public bool? dayMonday { get; set; }

        public bool? dayTuesday { get; set; }

        public bool? dayWednesday { get; set; }

        public bool? dayThursday { get; set; }

        public bool? dayFriday { get; set; }

        public bool? daySaturday { get; set; }

        public bool? daySunday { get; set; }

        public int? Active { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? binWeek { get; set; }

        public int? userUpdate { get; set; }

        public DateTime? dateUpdate { get; set; }

    }
    public class Season_available_date
    {
        [Key]
        public int rowId { get; set; }

        public int seasonId { get; set; }

        public DateTime eDate { get; set; }
    }
}