﻿using ListComparing.Models;
using Microsoft.EntityFrameworkCore;

namespace ListComparing
{
    public class MappingContext : DbContext
    {
        public DbSet<TestV2RatePlanMapping> TestV2RatePlanMapping { get; set; }
        public DbSet<Client_list_lov> Client_list_lov { get; set; }
        public DbSet<Property_list_lov> Property_list_lov { get; set; }
        public DbSet<Rate_ref> Rate_ref { get; set; }
        public DbSet<Rate_roomClass_xref> Rate_roomClass_xref { get; set; }
        public DbSet<Rate_season_xref> Rate_season_xref { get; set; }
        public DbSet<Rate_source_xref> Rate_source_xref { get; set; }
        public DbSet<Season_ref> Season_ref { get; set; }
        public DbSet<Season_available_date> Season_available_date { get; set; }
        public DbSet<RatePlan_ref> RatePlan_ref { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=sql.inncenter.pms.inrd.io;Database=inncenter01p;User Id=sysinnroaduat; password=pre$a@E9;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Rate_roomClass_xref>().HasKey(r => new { r.rateId, r.roomClassId });
            modelBuilder.Entity<Rate_season_xref>().HasKey(r => new { r.rateId, r.seasonId });
            modelBuilder.Entity<Rate_source_xref>().HasKey(r => new { r.rateId, r.sourceID });
            modelBuilder.Entity<Property_list_lov>().HasKey(r => new { r.ListId, r.ClientId,r.PropertyId });

        }
    }
}