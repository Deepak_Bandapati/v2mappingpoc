﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ListComparing
{
    public class RoomclassRate
    {
        public int RateID { get; set; }
        public int RoomclassID { get; set; }
        public decimal? Amount { get; set; }
        public List<DateTime> Dates { get; set; }

        public RoomclassRate()
        {
            Dates = new List<DateTime>();
        }
    }

    public class Rate
    {
        public int RateID { get; set; }
        public int Amount { get; set; }
        public int RoomclassID { get; set; }
    }


    public static class Mock
    {
        public static List<DateTime> Rc1S1V2dateLIst = new List<DateTime>()
            {
                DateTime.Now.Date,
                DateTime.Now.AddDays(1).Date,
                DateTime.Now.AddDays(2).Date,
                DateTime.Now.AddDays(3).Date,
                DateTime.Now.AddDays(4).Date,
                DateTime.Now.AddDays(5).Date,
                DateTime.Now.AddDays(13).Date,
                DateTime.Now.AddDays(14).Date
            };
        public static List<DateTime> Rc1S2V2dateLIst = new List<DateTime>()
            {
                DateTime.Now.AddDays(6).Date,
                DateTime.Now.AddDays(7).Date,
                DateTime.Now.AddDays(8).Date,
                DateTime.Now.AddDays(9).Date,
                DateTime.Now.AddDays(10).Date,
                DateTime.Now.AddDays(11).Date,
                DateTime.Now.AddDays(12).Date,
            };
        public static List<DateTime> Rc5S3V2dateList = new List<DateTime>()
        {
                DateTime.Now.AddDays(20).Date,
                DateTime.Now.AddDays(21).Date,
                DateTime.Now.AddDays(22).Date,
                DateTime.Now.AddDays(23).Date
        };
        public static List<DateTime> InndateLIst = new List<DateTime>()
            {
                DateTime.Now.Date,
                DateTime.Now.AddDays(1).Date,
                DateTime.Now.AddDays(2).Date,
                DateTime.Now.AddDays(3).Date,
                DateTime.Now.AddDays(4).Date,
                DateTime.Now.AddDays(5).Date,
                DateTime.Now.AddDays(6).Date,
                DateTime.Now.AddDays(7).Date,
                DateTime.Now.AddDays(8).Date,
                DateTime.Now.AddDays(9).Date,
                DateTime.Now.AddDays(10).Date,
                DateTime.Now.AddDays(11).Date,
                DateTime.Now.AddDays(12).Date,
                DateTime.Now.AddDays(13).Date,
                DateTime.Now.AddDays(14).Date,
            };

        public static List<RoomclassRate> NewRates = new List<RoomclassRate>() {
            new RoomclassRate{Amount=100,RoomclassID=1,Dates=Rc1S1V2dateLIst},
            new RoomclassRate{Amount=200,RoomclassID=1,Dates=Rc1S2V2dateLIst},
            new RoomclassRate{Amount=300,RoomclassID=3,Dates=Rc1S1V2dateLIst},
            new RoomclassRate{Amount=500,RoomclassID=5,Dates=Rc5S3V2dateList},

        };

        public static List<RoomclassRate> OldRates = new List<RoomclassRate>() {
            new RoomclassRate{Amount=100,RoomclassID=1,Dates=InndateLIst,RateID=1},
            new RoomclassRate{Amount=100,RoomclassID=2,Dates=InndateLIst,RateID=2},
            new RoomclassRate{Amount=100,RoomclassID=3,Dates=InndateLIst,RateID=3},
        };

        public static List<Rate> RatePlanRates = new List<Rate>()
        {
            new Rate{Amount = 500,RateID =10, RoomclassID =5},
        };
    }

}