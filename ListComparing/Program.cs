﻿using ListComparing.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ListComparing
{
    class Program
    {
        static void Main(string[] args)
        {
            var newrates = Mock.NewRates;
            var oldrates = Mock.OldRates;

            var mapper = new RatesMapper();
            mapper.RunMapper(newrates, oldrates);
            Console.Read();
        }
    }

    public class RatesMapper
    {
        public List<RoomclassRate> ModifiedRoomclassRates = new List<RoomclassRate>();
        public List<RoomclassRate> NewRoomClassRates = new List<RoomclassRate>();
        public List<Rate> rates = Mock.RatePlanRates;
        MappingContext _MappingContext = new MappingContext();

        public void RunMapper(List<RoomclassRate> newRates, List<RoomclassRate> oldRates)
        {
            var UseJson = false;
            var DBUpdate = false;
            var _V2RatePlan = GenerateRatePlanObjfromJson();
            if (UseJson) {
                oldRates = Fetchv1RatePlan(_V2RatePlan);
                newRates = Fetchv2RatePlan(_V2RatePlan);
            }

            FilterModifiedSeasons(newRates, oldRates);
            FindNewRates(newRates, oldRates);
            //AttachToExistingRates(newRates, rates);
            foreach (var modifiedRate in ModifiedRoomclassRates)
            {
                Console.WriteLine("modifed rates -->");
                Console.WriteLine($"{modifiedRate.Amount},{modifiedRate.RoomclassID},{modifiedRate.RateID}");
                foreach (var Date in modifiedRate.Dates)
                {
                    Console.WriteLine(Date);
                }
            }

            foreach (var newRoomclassRate in NewRoomClassRates)
            {
                Console.WriteLine("New rates ");
                Console.WriteLine($"{newRoomclassRate.Amount},{newRoomclassRate.RoomclassID},{newRoomclassRate.RateID}");
                foreach (var Date in newRoomclassRate.Dates)
                {
                    Console.WriteLine(Date);
                }
            }
            if (DBUpdate)
            {
                ModifyRates(ModifiedRoomclassRates, _V2RatePlan);
                CreateNewRate(NewRoomClassRates, _V2RatePlan);
            }
        }


        #region Filtering

        public void FilterModifiedSeasons(List<RoomclassRate> newRates, List<RoomclassRate> oldRates)
        {
            foreach (var newRate in newRates)
            {
                var result = oldRates.Where(x => x.RoomclassID == newRate.RoomclassID && x.Amount == newRate.Amount).FirstOrDefault();
                if (result != null)
                    ModifiedRoomclassRates.Add(result);
            }
            FilterSeasonDays(ModifiedRoomclassRates, newRates);
        }
        public void FilterSeasonDays(List<RoomclassRate> exisitingRates, List<RoomclassRate> newrates)
        {
            foreach (var existingRate in exisitingRates)
            {
                var newSeasonDate = newrates.FirstOrDefault(x => x.RoomclassID == existingRate.RoomclassID && x.Amount == existingRate.Amount);

                AddOrDeleteDays(existingRate, newSeasonDate);
            }
        }
        public void AddOrDeleteDays(RoomclassRate existingRate, RoomclassRate newSeasonDate)
        {
            List<DateTime> RemoveDates = new List<DateTime>();
            foreach (var Day in existingRate.Dates)
            {
                if (!newSeasonDate.Dates.Contains(Day))
                {
                    RemoveDates.Add(Day);
                }
            }
            foreach (var date in RemoveDates)
            {
                existingRate.Dates.Remove(date);

            }
        }
        public void FindNewRates(List<RoomclassRate> newRates, List<RoomclassRate> oldRates)
        {
            foreach (var newRate in newRates)
            {
                var result = oldRates.Where(x => x.RoomclassID == newRate.RoomclassID && x.Amount != newRate.Amount).FirstOrDefault();
                if (result != null)
                {
                    NewRoomClassRates.Add(newRate);
                }
                if (!oldRates.Any(x => x.RoomclassID == newRate.RoomclassID))
                {
                    NewRoomClassRates.Add(newRate);
                }
            }
        }

        public void AttachToExistingRates(List<RoomclassRate> newRoomClassRates, List<Rate> RatesAccrossRatePlan)
        {
            List<RoomclassRate> existingRates = new List<RoomclassRate>();
            foreach (var NewRoomclassRate in newRoomClassRates)
            {
                var item = RatesAccrossRatePlan.FirstOrDefault(x => x.RoomclassID == NewRoomclassRate.RoomclassID && x.Amount == NewRoomclassRate.Amount);
                if (item != null)
                {
                    NewRoomclassRate.RateID = item.RateID;
                    ModifiedRoomclassRates.Add(NewRoomclassRate);
                    existingRates.Add(NewRoomclassRate);
                }
            }
            foreach (var rate in existingRates)
            {
                NewRoomClassRates.Remove(rate);
            }
        }
        #endregion 

        private List<RoomclassRate> Fetchv1RatePlan(RatePlan _ratePlan)
        {
            var V1RoomclassRate = new List<RoomclassRate>();
            var V1RatePlanId = GetV1RatePlanIdByV2RatePlanId(_ratePlan.ratePlanId);
            if (V1RatePlanId == 0)
            {
                InsertListManagement(GenerateRatePlanObjfromJson());
            }
            else
            {
                var V1Rates = _MappingContext.Rate_ref.Where(x => x.ratePlanID == V1RatePlanId && x.Active == 1 && x.rateTypeID == 1 && x.propertyid == _ratePlan.propertyId).ToList();
                foreach (var rate in V1Rates)
                {
                    var RateRoomClass = _MappingContext.Rate_roomClass_xref.Where(x => x.rateId == rate.rateId && x.Active == 1).Distinct().ToList();
                    var RateSeasons = (from rs in _MappingContext.Rate_season_xref
                                       join s in _MappingContext.Season_ref on rs.seasonId equals s.seasonId
                                       where rs.rateId == rate.rateId
                                       select new { s.dateStart }).AsEnumerable().Select(x => x.dateStart.Date).ToList();
                    foreach (var roomClass in RateRoomClass)
                    {
                        V1RoomclassRate.Add(new RoomclassRate
                        {
                            RoomclassID = roomClass.roomClassId,
                            Amount = rate.amount,
                            RateID = rate.rateId,
                            Dates = RateSeasons
                        });
                    }
                }
            }
            return V1RoomclassRate;
        }

        private static List<RoomclassRate> Fetchv2RatePlan(RatePlan _ratePlan)
        {
            var groupedData = new List<RoomclassRate>();
            foreach (var _season in _ratePlan.seasons)
            {
                foreach (var _roomClass in _season.seasonRates)
                {
                    var roomClass = new RoomclassRate
                    {
                        RoomclassID = _roomClass.roomClassId,
                        Amount = _roomClass.rate,
                        Dates = _season.seasonDates
                    };
                    groupedData.Add(roomClass);
                }
            }
            return groupedData;
        }

        private  RatePlan GenerateRatePlanObjfromJson()
        {
            using (StreamReader r = new StreamReader("../../../request.json"))
            {
                string json = r.ReadToEnd();
                return Newtonsoft.Json.JsonConvert.DeserializeObject<RatePlan>(json);
            }
        }

        private  int GetV1RatePlanIdByV2RatePlanId(int V2RatePlanID)
        {
            var V1RatePlan = _MappingContext.TestV2RatePlanMapping.Where(x => x.V2RatePlanId == V2RatePlanID).FirstOrDefault();
            return V1RatePlan != null ? V1RatePlan.V1RatePlanId : 0;
        }

        private int InsertListManagement(RatePlan _ratePlan)
        {
            using (var _context = new MappingContext())
            {
                using (var transaction = _context.Database.BeginTransaction())
                {

                    var rateplanList = _context.Client_list_lov.Where(x => x.ClientId == _ratePlan.clientId && x.ListType == 6 && x.ListName == _ratePlan.name && x.Active != 2).ToList();
                    var rateplanCount = _context.Client_list_lov.Where(x => x.ClientId == _ratePlan.clientId && x.ListType == 6 && x.Active != 2).ToList();
                    var ClientListLov = new Client_list_lov();
                    var _listvalue = 0;
                    try
                    {
                        if (rateplanList.Count == 0)
                        {
                            _listvalue = rateplanCount.Max(x => x.ListValue) + 1;
                            ClientListLov.ClientId = _ratePlan.clientId;
                            ClientListLov.ListName = _ratePlan.name;
                            ClientListLov.ListDesc = _ratePlan.name;
                            ClientListLov.ListValue = _listvalue;
                            ClientListLov.ListType = 6;
                            ClientListLov.isSystemList = false;
                            ClientListLov.Active = 1;
                            ClientListLov.UserUpdate = 1234;
                            ClientListLov.dateUpdate = DateTime.Now;
                            _context.Add<Client_list_lov>(ClientListLov);
                            _context.SaveChanges();
                            var PropertyListLov = new Property_list_lov
                            {
                                ListId = ClientListLov.ListId,
                                ClientId = _ratePlan.clientId,
                                Active = true,
                                PropertyId = _ratePlan.propertyId,
                                UserUpdate = 1234,
                                dateUpdate = DateTime.Now
                            };
                            _context.Add<Property_list_lov>(PropertyListLov);
                            _context.SaveChanges();

                            var RatePlanRef = new RatePlan_ref
                            {
                                clientID = _ratePlan.clientId,
                                mealPlanID = 13,
                                ratePlanID = _listvalue
                            };
                            _context.Add<RatePlan_ref>(RatePlanRef);

                            var V2RatePlanMapping = new TestV2RatePlanMapping
                            {
                                V1RatePlanId = _listvalue,
                                V2RatePlanId = _ratePlan.ratePlanId
                            };
                            _context.Add<TestV2RatePlanMapping>(V2RatePlanMapping);
                            _context.SaveChanges();
                            transaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        transaction.Dispose();
                    }
                    return _listvalue;
                }
            }
        }
        public void CreateNewRate(List<RoomclassRate> _roomClassRate, RatePlan _rateplan)
        {
            using (var _context = new MappingContext()) {
                using (var transaction = _context.Database.BeginTransaction())
                {
                    try
                    {
                        List<Rate_ref> NewRatesList = new List<Rate_ref>();
                        List<Rate_roomClass_xref> RateRoomClasses = new List<Rate_roomClass_xref>();
                        foreach (var newRoomclassRate in _roomClassRate)
                        {
                            Rate_ref newRate = getRateObject(newRoomclassRate, _rateplan);
                            _context.Add<Rate_ref>(newRate);
                            _context.SaveChanges();
                            Console.WriteLine("Rate_ref Created");
                            NewRatesList.Add(newRate);

                            var newRateRoomclass = new Rate_roomClass_xref
                            {
                                rateId = newRate.rateId,
                                roomClassId = newRoomclassRate.RoomclassID,
                                propertyID = _rateplan.propertyId,
                                Active = 1
                            };
                            _context.Add<Rate_roomClass_xref>(newRateRoomclass);
                            _context.SaveChanges();
                            Console.WriteLine("Rate_roomClass_xref Created");
                            RateRoomClasses.Add(newRateRoomclass);

                            List<Season_ref> OldSeasons = new List<Season_ref>();
                            OldSeasons = _context.Season_ref.Where(x => x.dateEnd == Convert.ToDateTime(x.dateStart).AddDays(1) && x.propertyid == _rateplan.propertyId && x.Active == 1).ToList();
                            foreach (var seasonDate in newRoomclassRate.Dates)
                            {
                                var ExistingSeasonForDay = OldSeasons.Where(x => x.dateStart == seasonDate.Date && x.dateEnd == seasonDate.AddDays(1).Date).FirstOrDefault();
                                var newSeason = new Season_ref
                                {
                                    Active = 1,
                                    dateStart = seasonDate.Date,
                                    dateEnd = seasonDate.AddDays(1).Date,
                                    dayFriday = true,
                                    dayMonday = true,
                                    daySaturday = true,
                                    daySunday = true,
                                    dayThursday = true,
                                    dayTuesday = true,
                                    dayWednesday = true,
                                    propertyid = _rateplan.propertyId,
                                    seasonName = seasonDate.ToShortDateString(),
                                    userUpdate = 1234,
                                    dateUpdate = DateTime.Now
                                };
                                if (ExistingSeasonForDay != null)
                                {
                                    var oldRateSeasons = _context.Rate_season_xref.Where(x => x.propertyID ==_rateplan.propertyId && x.seasonId == ExistingSeasonForDay.seasonId && x.Active == 1).ToList();

                                    foreach (var rateSeason in oldRateSeasons)
                                    {
                                        rateSeason.Active = 0;
                                        _context.Update<Rate_season_xref>(rateSeason);
                                        _context.SaveChanges();
                                    }
                                }
                                else
                                {
                                    _context.Add<Season_ref>(newSeason);
                                    _context.SaveChanges();
                                    Console.WriteLine("Season_ref Created");
                                    _context.Add<Season_available_date>(new Season_available_date { eDate = seasonDate.Date, seasonId = newSeason.seasonId });
                                    _context.SaveChanges();
                                    Console.WriteLine("Season_available_date Created");
                                    ExistingSeasonForDay = newSeason;
                                }
                                _context.Add<Rate_season_xref>(new Rate_season_xref
                                {
                                    propertyID = _rateplan.propertyId,
                                    rateId = newRate.rateId,
                                    seasonId = ExistingSeasonForDay.seasonId,
                                    Active = 1
                                });
                                _context.SaveChanges();
                                Console.WriteLine("Rate_season_xref Created");
                            }
                        }

                        foreach (var channel in _rateplan.ratePlanChannels)
                        {
                            foreach (var rate in NewRatesList)
                            {
                                _context.Add<Rate_source_xref>(new Rate_source_xref
                                {
                                    rateId = rate.rateId,
                                    sourceID = channel.channelId,
                                    propertyID = _rateplan.propertyId,
                                    Active = 1
                                });
                                _context.SaveChanges();
                                Console.WriteLine("Rate_source_xref Created");
                            }
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex) {
                        transaction.Rollback();
                        transaction.Dispose();
                    }
                }
            }
        }


        public void ModifyRates(List<RoomclassRate> modifiedRoomclassRates, RatePlan v2RatePlan)
        {
           
        }


        private Rate_ref getRateObject(RoomclassRate _roomClassRate, RatePlan _rateplan)
        {
            return new Rate_ref()
            {
                propertyid = _rateplan.propertyId,
                rateName = _rateplan.name,
                rateTypeID = _rateplan.ratePlanTypeId,
                rateDescription = _rateplan.name,
                rateDisplayName = _rateplan.name,
                amount = _roomClassRate.Amount,
                perAddAdult = 0,
                perAddChild = 0,
                ratePlanID = GetV1RatePlanIdByV2RatePlanId(_rateplan.ratePlanId),
                rackRate = true,
                Active = 1,
                isProrated = false,
                rateInterval = 1,
                allowStayThrough = false,
                minStay = 0,
                maxAdults = 2,
                maxPersons = 4,
                lowestRate = true,
                userUpdate=0,
                dateUpdate=DateTime.Now
            };
        }
    }

}
